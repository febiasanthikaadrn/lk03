/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Dalam Konteks kalkulator, interface lebih cocok digunakan daripada kelas abstrak dalan beberapa kondisi,
    seperti :
    - jika terdapat banyak jenis yang berbeda dan tidak ada metode bersama yang dapat diimplementasikan di
      semua jenis operasi. Dalam kalkulator, terdapat jenis operasi matematika yang berbeda seperti penambahan,
      pengurangan, perkalian, pembagian, dan sebagainya. Dalam kasus seperti ini, menggunakan interface akan
      memungkinkan setiap jenis operasi untuk mengimplementasikan metode-metode mereka sendiri sesuai dengan
      logika operasi masing-masing. Setiap operasi dapat memiliki implementasi yang unik untuk metode hitung
      dalam interface Kalkulator.
    - Jika kalkulator ingin mendukung multiple inheritance. Dalam beberapa kasus, mungkin ada kebutuhan untuk
      sebuah kalkulator untuk memiliki fungsionalitas yang diperoleh dari beberapa sumber yang berbeda."
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}