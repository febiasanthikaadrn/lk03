/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Dalam konteks kalkulator, penggunaan kelas abstrak lebih cocok daripada interface ketika ada beberapa
    metode bersama yang dapat diimplementasikan di kelas abstrak tersebut. Dalam kalkulator ini, kelas abstrak
    Kalkulator memiliki metode setOperan yang mengatur nilai dari kedua operan dan metode abstrak hitung yang
    akan diimplementasikan oleh kelas turunan untuk melakukan perhitungan. Namun, jika hanya ada satu metode
    yang perlu diimplementasikan dan tidak ada hubungan erat antara kelas-kelas lainnya, menggunakan interface
    dapat menjadi pilihan yang lebih baik. Interface memberikan fleksibilitas yang lebih besar karena sebuah
    kelas dapat mengimplementasikan beberapa antarmuka sekaligus."
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}